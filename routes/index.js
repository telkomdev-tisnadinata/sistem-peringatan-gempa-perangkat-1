var express = require('express');
var router = express.Router();
const device = require('./device');
const cronjob = require('./cronjob');
const moment = require('moment');

router.get('/', device.showStatus);
router.get('/device/sinyal', device.getStatus);
router.post('/device/sinyal', device.addDevice);
router.put('/device/sinyal', createRequestLog, device.updateStatus);
router.get('/device/sinyal/new', createRequestLog, device.updateStatus);


async function createRequestLog(req, res, next) {
  const requestId = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
  const dikirim = moment(req.query.requestTime) || moment(new Date().getTime());
  const diterima = moment(new Date().getTime());
  req.requestId = requestId;
  global.rekapWaktu[requestId] = {
      id: requestId,
      data_dikirim: dikirim,
      data_diterima: diterima,
      data_update_selesai: 0,
      fuzzy_selesai: 0,
      proses_selesai: 0
  }
  next();
}

cronjob.start();

module.exports = router;
