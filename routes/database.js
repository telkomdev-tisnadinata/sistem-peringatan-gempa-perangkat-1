const mysql      = require('mysql');
const connection = mysql.createConnection({
  connectionLimit: 100,
  host     : process.env.DB_HOST || 'localhost',
  user     : process.env.DB_USER || 'root',
  password : process.env.DB_PASSWORD || '',
  database : process.env.DB_NAME || 'db_gempa'
});
async function startConnection(){
  connection.connect();  
}
async function endConnection(){
  connection.end();  
}
async function insertProsesTime (payload) {
  return  new Promise(function(resolve, reject){
    const sql = `INSERT INTO proses_time(location,data_dikirim,data_diterima,data_update_selesai,fuzzy_selesai,proses_selesai,lama_waktu)
        VALUES('${payload.location}','${payload.data_dikirim}','${payload.data_diterima}','${payload.data_update_selesai}',
        '${payload.fuzzy_selesai}','${payload.proses_selesai}',${payload.lama_waktu})`
    connection.query(sql, function (error, results, fields) {
      if (error) {
        reject(error);
      }
      resolve(results);
    });
  });
}

async function getNodeList (location) {
  return  new Promise(function(resolve, reject){
    const sql = `SELECT * FROM data_sensor WHERE location='${location}'`;
    connection.query(sql, function (error, results, fields) {
      if (error) {
        reject(error);
      }
      resolve(results);
    });
  });
}
 
async function getNode (payload) {
  return  new Promise(function(resolve, reject){
    const sql = `SELECT * FROM data_sensor WHERE node_name='${payload.node_name}'`;
    connection.query(sql, function (error, results, fields) {
      if (error) {
        reject(error);
      }
      resolve(results);
    });
  });
}

async function updateNode (payload) {
  return  new Promise(function(resolve, reject){
    const sql = `UPDATE data_sensor SET earthquake_strength='${payload.earthquake_strength}'
        WHERE node_name='${payload.node_name}'`;
    connection.query(sql, function (error, results, fields) {
      if (error) {
        reject(error);
      }
      resolve(results);
    });
  });
}

async function insertNode (payload) {
  return  new Promise(function(resolve, reject){
    const sql = `INSERT INTO data_sensor(node_name,earthquake_strength,location)
        VALUES ('${payload.node_name}', '${payload.earthquake_strength}', '${process.env.SERVER}')`;
    connection.query(sql, function (error, results, fields) {
      if (error) {
        reject(error);
      }
      resolve(results);
    });
  });
}

async function insertNodeHistory (payload) {
  return  new Promise(function(resolve, reject){
    const sql = `INSERT INTO data_sensor_history(node_name,earthquake_strength)
        VALUES ('${payload.node_name}', '${payload.earthquake_strength}')`;
    connection.query(sql, function (error, results, fields) {
      if (error) {
        reject(error);
      }
      resolve(results);
    });
  });
}

module.exports = {
  insertProsesTime,
  getNodeList,
  insertNode,
  getNode,
  updateNode,
  insertNodeHistory
}