async function fuzzyGempa(kekuatanGempa) {
  const sr = parseFloat(kekuatanGempa.replace(",", "."));
  if (sr > 0 && sr <= 4.0) {
    return "low";
  } else if (sr > 4.0 && sr < 5.0) {
    // nilai2 = (x – a) / (b - a)"
    // nilai1 = (d – x) / (d - c)"
    const nilai2 =  (sr - 4.0) / (5.0 - 4.0);
    const nilai1 =  (5.0 - sr) / (5.0 - 4.0);
    return (nilai1 > nilai2) ? "low" : "medium";
  } else if (sr >= 5.0 && sr <= 5.8) {
    return "medium";
  } else if (sr > 5.8 && sr < 6.2) {
    // nilai2 = (x – a) / (b - a)"
    // nilai1 = (d – x) / (d - c)"
    const nilai2 =  (sr - 5.8) / (6.2 - 5.8);
    const nilai1 =  (6.2 - sr) / (6.2 - 5.8);
    return (nilai1 > nilai2) ? "medium" : "high";
  } else if (sr >= 6.2) {
    return "high";
  } else {
    return "undefined";
  }
}
async function fuzzySinyal(presentaseSinyal) {
  const ps = parseFloat(presentaseSinyal.replace(",", "."));
  if (ps > 0 && ps <= 35.0) {
    return "sedikit";
  } else if (ps > 35.0 && ps < 45.0) {
    // nilai2 = (x – a) / (b - a)"
    // nilai1 = (d – x) / (d - c)"
    const nilai2 =  (ps - 35.0) / (45.0 - 35.0);
    const nilai1 =  (45.0 - ps) / (45.0 - 35.0);
    return (nilai1 > nilai2) ? "sedikit" : "cukup";
  } else if (ps >= 45.0 && ps <= 55.0) {
    return "cukup";
  } else if (ps > 55.0 && ps < 75.0) {
    // nilai2 = (x – a) / (b - a)"
    // nilai1 = (d – x) / (d - c)"
    const nilai2 =  (ps - 55.0) / (75.0 - 55.0);
    const nilai1 =  (75.0 - ps) / (75.0 - 55.0);
    return (nilai1 > nilai2) ? "cukup" : "banyak";
  } else if (ps >= 75.0) {
    return "banyak";
  } else {
    return "undefined";
  }
}
async function getFuzzyResult(sinyal, kekuatan) {
  let result; // 0 = false, 1 = true, 2 = undefined
  if (sinyal === "sedikit") {
    if (kekuatan === "low" || kekuatan === "medium" || kekuatan === "high") {
      result = 0;
    } else {
      result = 2;
    }
  } else if (sinyal === "cukup") {
    if (kekuatan === "low") {
      result = 0;
    } else if (kekuatan === "medium" || kekuatan === "high") {
      result = 1;
    } else {
      result = 2;
    }
  } else if (sinyal === "banyak") {
    if (kekuatan === "low" || kekuatan === "medium" || kekuatan === "high") {
      result = 1;
    } else {
      result = 2;
    }
  } else {
    result = 2;
  }
  return result;
}

module.exports = {
  fuzzyGempa,
  fuzzySinyal,
  getFuzzyResult
}