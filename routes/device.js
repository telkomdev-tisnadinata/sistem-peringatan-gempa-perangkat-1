const sortJsonArray = require('sort-json-array');
const fuzzy = require('./fuzzy');
const notifikasi = require('./notifikasi');
const moment = require('moment');
const db = require('./database');

async function addDevice (req, res) {
  const result = {
    success: true,
    error: false,
    data: []
  };
  const getInsertResult = req.body.map(async newDevice => {
    try {
      const insertDevice = await db.insertNode(newDevice);
      if (insertDevice.affectedRows > 0) {
        return newDevice;
      }
      return { error: 'Gagal menambahkan' }
    } catch (error) {
      return { error: error.message };
    }
  })
  result.data = await Promise.all(getInsertResult);
  global.dataVersion = new Date().toLocaleString();
  console.log('Add device : ' + JSON.stringify(result));
  res.send(result);
};

async function updateStatus (req, res) {
  const result = {
    success: true,
    error: false,
    data: []
  };
  if (req.query.node_name) {
    req.body = [req.query];
  }
  let haveUpdate = false;
  const getUpdateResult = req.body.map(async body => {
    try {
      const getNode = await db.getNode(body);
      if (getNode.length > 0) {
        const updateNode = await db.updateNode(body);
        if (updateNode.affectedRows > 0) {
          await db.insertNodeHistory(body);
        }
        const lastKategori = await fuzzy.fuzzyGempa(String(getNode[0].earthquake_strength));
        const newKategori = await fuzzy.fuzzyGempa(String(body.earthquake_strength));
        if (getNode[0].earthquake_strength !== body.earthquake_strength) haveUpdate = true;
        return getNode[0];
      }
      return { error: 'Node tidak ditemukan' }
    } catch (error) {
      return { error: error.message };      
    }
  });
  result.data = await Promise.all(getUpdateResult);
  if (haveUpdate) {
    global.dataVersion = new Date().toLocaleString();
    global.rekapWaktu[req.requestId].data_update_selesai = moment(new Date().getTime());
    jalankanFuzzy(req.requestId);
  }
  console.log('Update Status : ' + JSON.stringify(result));
  res.send(result);
};

async function getStatus (req, res) {
  res.render('response', { data: global.dataVersion });
}

async function showStatus (req, res) {
  let finalResult;
  try {
    const getNodeList = await db.getNodeList(process.env.SERVER);
    let result = {
      data: [],
      totalSinyal: 0,
      totalNode: getNodeList.length,
      presentaseKemunculan: 0,
      hasilDeteksi: "TIDAK TERJADI GEMPA",
      hostUrl: process.env.HOST_URL || `http://localhost:${process.env.PORT}`,
      kekuatanGempa: "",
      kategoriGempa: "",
      kategoriSinyal: "",
      dataVersion: global.dataVersion
    }
    let gempaLow = [];
    let gempaMedium = [];
    let gempaHigh = [];
    // mengambil semua data node
    getNodeList.map(async device => {
      if (device.earthquake_strength > 2.9) {
        result.totalSinyal++;
        // konversi nilai kekuatan gempa ke kategori gempa
        const getFuzzyGempa = await fuzzy.fuzzyGempa(String(device.earthquake_strength));
        if (getFuzzyGempa === "low") {
          device.earthquake_status =  "yellow";
          gempaLow.push(device.earthquake_strength);
        } else if (getFuzzyGempa === "medium") {
          device.earthquake_status =  "orange";
          gempaMedium.push(device.earthquake_strength);
        } else if (getFuzzyGempa === "high") {
          device.earthquake_status =  "red";
          gempaHigh.push(device.earthquake_strength);
        } else {
          device.earthquake_status =  "grey";
        }
      } else {
        device.earthquake_status = "green";
      }
      device.skala_sig = await getSkalaSIGBMKG(device.earthquake_strength);
      result.data.push(device);
    })

    // menghitung presentase kemunculan sinyal,
    result.presentaseKemunculan = Math.floor((result.totalSinyal/result.totalNode) * 100);
    // konversi nilai kemunculan sinyal ke kategori sinyal
    const kategoriSinyal = await fuzzy.fuzzySinyal(String(result.presentaseKemunculan));
    // mencari kategori gempa yang terbanyak
    let kategoriGempa = await getKategoriGempa(gempaLow, gempaMedium, gempaHigh);
    // menghitung rata-rata kekuatan yang dikirim alat
    var sum = 0;
    for( var i = 0; i < kategoriGempa.kekuatan.length; i++ ){
        sum += kategoriGempa.kekuatan[i];
    }
    var avgKekuatan = parseFloat(sum/kategoriGempa.kekuatan.length).toFixed(1);
    kategoriGempa.kategori = await fuzzy.fuzzyGempa(avgKekuatan);
    // mencari hasil akhir dari rule base yang ditentukan berasarkan kategori sinyal dan gempa
    const hasilDeteksi = await fuzzy.getFuzzyResult(kategoriSinyal, kategoriGempa.kategori);
    if (hasilDeteksi == 1) {
      result.hasilDeteksi = "TERJADI GEMPA";
      // result.kekuatanGempa = "Kekuatan "+Math.min.apply(null, kategoriGempa.kekuatan)+" - "+Math.max.apply(null, kategoriGempa.kekuatan)+" SR";
      // result.kekuatanGempa = "Kekuatan Sekitar "+avgKekuatan+" SR";
      result.kekuatanGempa = "SKALA SIG BMKG : "+await getSkalaSIGBMKG(avgKekuatan);
      result.avgKekuatan = avgKekuatan;
    } else {
      hasilDeteksi.hasilDeteksi = "TIDAK TERJADI GEMPA";
      result.kekuatanGempa = "";
      result.avgKekuatan = 0.0;
    }
    result.kategoriSinyal = kategoriSinyal;
    result.kategoriGempa = kategoriGempa.kategori;
    await Promise.all(result.data)
    sortJsonArray(result.data, 'node_name', 'asc');
    finalResult = result;
  } catch (error) {
    finalResult = error.message;
    console.log(error.message)
  }
  finalResult.serverName = process.env.SERVER;
  if (req === 'dari update' || req === 'cronjob') {
    console.log(req);
    return finalResult;
  }
  res.render('index', finalResult);
}

async function getKategoriGempa (gempaLow, gempaMedium, gempaHigh) {
  let result = {
    kategori: "",
    kekuatan: ""
  };
  result.kekuatan = gempaLow.concat(gempaMedium.concat(gempaHigh));
  if (gempaLow.length != 0 && gempaLow.length > gempaMedium.length && gempaLow.length > gempaHigh.length) {
    result.kategori = "low";
  } else if (gempaMedium.length != 0 && gempaMedium.length >= gempaLow.length && gempaMedium.length > gempaHigh.length) {
    result.kategori = "medium";
  } else if (gempaHigh.length != 0 && gempaHigh.length >= gempaLow.length && gempaHigh.length >= gempaMedium.length) {
    result.kategori = "high";
  } else {
    result.kategori = "undefined";
    result.kekuatan = [0]
  }
  return result;
}
async function getSkalaSIGBMKG (kekuatanGempa) {
  // refereensi http://blog.umy.ac.id/restufaizah/skalla-dan-parameter-gempa-bumi/ http://blog.umy.ac.id/restufaizah/files/2012/07/mmi1.gif
  // refereensi https://www.bmkg.go.id/gempabumi/skala-mmi.bmkg
  let skala = "Not Defined";
  if (kekuatanGempa < 2.2) {
    skala = "I";
  } else if (kekuatanGempa < 4.1) {
    skala = "II";
  } else if (kekuatanGempa < 4.6) {
    skala = "III";
  } else if (kekuatanGempa < 5.9) {
    skala = "IV";
  } else if (kekuatanGempa >= 5.9) {
    skala = "V";
  }
  return skala;
}
async function jalankanFuzzy(requestId) {
  const res = {
    render: (page, result) => {
      if (result.hasilDeteksi === "TERJADI GEMPA") {
        global.notification = true;
        global.pesan = result.hasilDeteksi+". "+result.kekuatanGempa
      } else {
        global.notification = false;
      }
    }
  }
  await showStatus("dari update", res);
  if (global.notification) {
    console.log('MENGIRIM NOTIFIKASI');
    // console.log(await notifikasi.sendSMS());
    // console.log(await notifikasi.sendEMAIL());
  }
  global.rekapWaktu[requestId].fuzzy_selesai = moment(new Date().getTime());
  simpanRekapWaktu(requestId);
}

async function simpanRekapWaktu(requestId) {
  try {
    const rekapWaktu = global.rekapWaktu[requestId];
    rekapWaktu.proses_selesai = moment(new Date().getTime());
    const body = {
      location: process.env.SERVER,
      data_dikirim: moment(rekapWaktu.data_dikirim).valueOf(),
      data_diterima: moment(rekapWaktu.data_diterima).valueOf(),
      data_update_selesai: moment(rekapWaktu.data_update_selesai).valueOf(),
      fuzzy_selesai: moment(rekapWaktu.fuzzy_selesai).valueOf(),
      proses_selesai: moment(rekapWaktu.proses_selesai).valueOf(),
      lama_waktu: moment(rekapWaktu.proses_selesai).diff(moment(rekapWaktu.data_diterima))
    }
    const insertDevice = await db.insertProsesTime(body);
    if (insertDevice.affectedRows > 0) {
      console.log('Rekap waktu berhasil...');
    } else {
      console.log('Rekap waktu gagal...');
    }
    delete global.rekapWaktu[requestId];
  } catch (error) {
    console.log(error.message);
  }
}

module.exports = {
  addDevice,
  updateStatus,
  showStatus,
  getStatus
};
